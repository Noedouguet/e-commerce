const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const mySchema = new Schema({
    name: { type: String, required: true },
    auteur: { type: String, required: true },
    ISBN: { type: String, required: true },
    editeur: { type: String, required: true },
    genre: { type: String, required: true },
    prix: { type: Number, required: true },
    datePublication: { type: Date, required: true },
    image: {type: String, required: true}
});


let livres = mongoose.model("livres", mySchema);

module.exports = livres;