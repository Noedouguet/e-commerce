/*Global $, console, alert*/
$(document).ready(function () {
    "use strict";

    // Set height Header Equal Window Height
    $('.header').height($(window).height());

    //Adding Class Fixed
    $('header').wrapInner("<div class='inner'></div>");
    $(window).scroll(function () {
        if ($(this).scrollTop() > 5) {
            $('.header header').addClass('fixed');
        } else {
            $('.header header ').removeClass('fixed');
        }
    });

    // Trigger typed Js
    $('.typed').typed({
        strings: ['Voici notre site de E-Commerce sur les livres <br />Il est réalisé par Denis, Noé et Andrews, le trio de choc'],
        typeSpeed: 25,
        showCursor: false,
        loop: true
    });

    // Centring
    $('.h_info').css({
        paddingTop: (($(window).height() - $('.header .h_info').height()) / 2) - 30
    });

    // Trigger Nice Scroll
    $('body').niceScroll({
        cursorcolor:'#24b662',
        cursorwidth: '7px',
        cursorborder: 'none'
    });

    $('.header li.go a').click(function (e) {
        e.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    $('header li a').click(function (e) {
      // Récupérez l'URL de destination depuis l'attribut href du lien
      var destination = $(this).attr('href');
  
      // Vérifiez si l'URL de destination est égale à '/boutique'
      if (destination === '/boutique' || destination === '/compte' || destination === '/panier'|| destination === '/login' || destination === '/logout' || destination === '/myAccount') {
          // Laissez le lien fonctionner normalement
          return;
      }
  
      // Empêchez la navigation par défaut pour les autres liens
      e.preventDefault();
  
      // Effectuez le défilement en douceur vers la section correspondante
      $('html, body').animate({
          scrollTop: $('#' + $(this).data('target')).offset().top
      }, 1000);
  });


    $('.portfolio .container ul li > a').click(function (e) {
        e.preventDefault();
    });

    $('.portfolio .container ul li  a').click(function () {
        $(this).parent('li').addClass('active').siblings('li').removeClass('active');
    });

    // Trigger Mix It Up
    $('.portfolio .container > .container ').mixItUp();

    // Trigger FancyBox
    $('.mix a').fancybox();

    // Go  To Portfolio
    $('.h_info a').click(function () {
        $('html, body').animate({
            scrollTop: $('.portfolio').offset().top
        }, 1000);
    });

    // Trigger BxSlider
    $('.bxslider').bxSlider({
        controls: false
    });

    // Trigger Count To
    $(window).scroll(function () {
        if (($(this).scrollTop() >= $('.count').offset().top) - 30) {
            $('.timer').countTo();
        }
    });

    // Go To Top
    if ($(window).scrollTop() >= $('.about').offset().top) {
        $('.top').fadeIn();
    } else {
        $('.top').fadeOut();
    }
    $(window).scroll(function () {
        if ($(window).scrollTop() >= $('.about').offset().top) {
            $('.top').fadeIn();
        } else {
            $('.top').fadeOut();
        }
    });

    $('.top').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1500);
    });

});