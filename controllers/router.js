const express   = require('express');
let router      = express.Router();
const Users    = require('../models/users');
const bcrypt = require('bcrypt');
const users = require('../models/users');

const isLogin = (req, res, next) => {
    if(req.session && req.session.user) {
        next()
    } else {
        res.redirect('/accueil')
    }
}
router.get("/login",async(req,res)=>{
    res.render("login")
})
// router.get('/', isLogin, async (req, res) => {
//     res.render('accueil', { user: req.session.user })
// })


// Read All
router.get('/users', async(req, res) => {
    const users = await Users.find()
    res.json(users)
})
// Read One by Email ( body )
router.get('/user', async (req, res) => {
    const email = req.body.email
    const user = await Users.findOne({ email: email })
    res.json(user)
})
router.get('/userAdmin',async(req,res)=>{
    var admin= true;
    res.render("register",{admin})
})
// Create One ( body )
router.post('/userAdmin', async (req, res) => {
    if(req.body.username === undefined || req.body.email === undefined|| req.body.password === undefined){
        return res.status(422).send(false)
    }
    const user = new Users({username: req.body.username, email: req.body.email, password: req.body.password,access:"admin"})
    await user.save()
    return res.status(200).redirect("/login");
})

// Update One by Email ( body )
router.post('/userUpdate', async (req, res) => {
    //if(req.body.email === undefined) {return res.status(422).send(false);}
    const email = req.body.email;
    const user = await Users.updateOne({ email: email }, { username: req.body.username })
    return res.status(200).redirect('/myAccount');
})


router.post('/login', async (req, res) => {
    var email = req.body.email
    var pwd = req.body.password
    
   var user =  await users.findOne({email : email})
    console.log(user)
   if(user.email === undefined){

   }
   else{
    bcrypt.compare(pwd,user.password,function(err,results){
        if(err)throw err;

        console.log(results);
        if(results){
            req.session.user = req.body;
            res.status(200).redirect("/");
        }else{
            res.status(422).send(false);
        }

    })

        
   }

})

router.get('/register', async (req, res) => {
    var admin= false;
    res.render('register',{admin})
})

router.post('/register', async (req, res) => {
    var email= req.body.email
    var username = req.body.username
    var pwd = req.body.password
    var hashedPwd = await bcrypt.hash(pwd,10);
    console.log(hashedPwd);

    //ajout a la bdd
    let d = new Users({
        username:  username,
        email: email,
        password: hashedPwd,
        access:"guest"
    })
    await d.save();

    res.send(200).render("login");

})

router.post('/deleteUser',async(req,res)=>{
    var emaild =req.session.user.email;
    if( emaild !==undefined){
        const email= emaild
    
        await users.deleteOne({email: email})
        return res.status(200).redirect("/");
    }else{
        return res.status(422).redirect("/");
    }

})

router.get("/myAccount",async(req,res)=>{
    const email = req.session.user.email;
    const user = await Users.findOne({email : email })
    const username = user.username;
    
    res.render("myAccount",{username, email});

})


router.get('/logout', async (req, res) => {
    req.session.destroy();
    res.redirect('/')
})

module.exports = router;