const express   = require('express');
let routerAccueil      = express.Router();
const Users    = require('../models/users');
const bcrypt = require('bcrypt');
const Livres = require('../models/livres'); // Le chemin peut varier en fonction de la structure de votre projet



routerAccueil.get('/', async (req, res) => {
    try {
        // Récupérez les données des livres depuis la base de données
        const livres = await Livres.find({});
        const user = req.session.user;
        console.log(user);
        res.render('accueil', { livres,user }); // Transmettez les données des livres au modèle.
    } catch (err) {
        res.status(500).json({ error: err.message });
    }
})

routerAccueil.get('/boutique', async (req, res) => {
  try {
    const livres = await Livres.find({});
    const user = req.session.user;
    res.render('boutique', { livres,user }); // Send only the 'livres' data to the view.
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

// Ajoutez cette route à votre fichier de routes
routerAccueil.get('/livresListe', async (req, res) => {
  try {
    const livres = await Livres.find({});
    const user = req.session.user;
    res.render('livresListe', { livres,user }); // Send only the 'livres' data to the view.
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

routerAccueil.get('/livre/:id', async (req, res) => {
  const user = req.session.user;
  try {
    const livre = await Livres.findById(req.params.id);
    if (!livre) {
      return res.status(404).send('Livre non trouvé.');
    }
    res.render('produit', { livre,user });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});


// Ajoutez cette route à votre fichier de routes
routerAccueil.get('/livresListe', async (req, res) => {
  const user = req.session.user;
  try {
    const livres = await Livres.find({});
    res.render('livresListe', { livres,user }); // Send only the 'livres' data to the view.
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});

routerAccueil.get('/livre/:id', async (req, res) => {
  const user = req.session.user;
  try {
    const livre = await Livres.findById(req.params.id);
    if (!livre) {
      return res.status(404).send('Livre non trouvé.');
    }
    res.render('produit', { livre,user });
  } catch (err) {
    res.status(500).json({ error: err.message });
  }
});


module.exports = routerAccueil;