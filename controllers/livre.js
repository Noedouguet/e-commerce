const express   = require('express');
let router      = express.Router();
const Livres    = require('../models/livres');

// Read All
router.get('/livres', async(req, res) => {
    const livres = await Livres.find()
    res.json(livres)
})

// Read One by Name ( body )
router.get('/livre', async (req, res) => {
    const name = req.body.name
    const livre = await Livres.findOne({ name: name })
    res.json(livre)
})

// Create One ( body )
router.post('/livre', async (req, res) => {
    const livre = new Livres({
        name: req.body.name,
        auteur: req.body.auteur, 
        ISBN: req.body.ISBN,
        editeur: req.body.editeur,
        genre: req.body.genre,
        prix: req.body.prix,
        datePublication: req.body.datePublication,
        image: req.body.image
    });
    await livre.save()
    res.json(livre)
})

// Delete One by Name ( body )
router.delete('/livre', async (req, res) => {
    const name = req.body.name
    const livre = await Livres.deleteOne({ name: name })
    res.json(livre)
})

// Update One by Name ( body )
router.put('/livre', async (req, res) => {
    const name = req.body.name
    const livre = await Livres.updateOne({ name: name }, {
        auteur: req.body.auteur, 
        ISBN: req.body.ISBN,
        editeur: req.body.editeur,
        genre: req.body.genre,
        prix: req.body.prix,
        datePublication: req.body.datePublication,
        image: req.body.image
    })
    res.json(livre)
})

module.exports = router;